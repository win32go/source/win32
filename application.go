/* application.go */

package win32

import (
	"log"
	"sync"
	"syscall"
	"unsafe"
	"gitlab.com/win32go/source/win32/w32"
)


// Window Stack
type goStack struct {
	index 			map[w32.HWND] int
	windows 		map[int] GoObject
	controls 		map[int] GoObject
	controlfocus	int
	windowfocus 	int
	mu      		sync.Mutex
}

func (s *goStack) topWindow() GoObject {
	s.mu.Lock()
	defer s.mu.Unlock()
	if len(s.windows) == 0 {
		return nil
	}
	return s.windows[0]
}

func (s *goStack) addControl(control GoObject, id int) {
	s.mu.Lock()
	defer s.mu.Unlock()
	s.controls[id] = control
	s.index[control.wid().hWnd] = id
	s.controlfocus = id
}

func (s *goStack) addWindow(window GoObject, id int) {
	s.mu.Lock()
	defer s.mu.Unlock()
	s.windows[id] = window
	s.index[window.wid().hWnd] = id
	s.windowfocus = id
}

func (s *goStack) focusControl() (control GoObject) {
	s.mu.Lock()
	defer s.mu.Unlock()
	if len(s.controls) == 0 {
		return nil
	}
	return s.controls[s.controlfocus]
}

func (s *goStack) focusWindow() (window GoObject) {
	s.mu.Lock()
	defer s.mu.Unlock()
	if len(s.windows) == 0 {
		return nil
	}
	return s.windows[s.windowfocus]
}

func (s *goStack) removeControl(id int) {
	s.mu.Lock()
	defer s.mu.Unlock()
	control := s.controls[id]
	hWnd := control.wid().hWnd
	delete(s.index, hWnd)
	delete(s.controls, id)
	if len(s.controls) > 0 {
		s.controlfocus = 0
	}
}

func (s *goStack) removeWindow(id int) {
	s.mu.Lock()
	defer s.mu.Unlock()
	window := s.windows[id]
	hWnd := window.wid().hWnd
	delete(s.index, hWnd)
	delete(s.windows, id)
	if len(s.windows) > 0 {
		s.windowfocus = 0
	}
}

func (s *goStack) isLastWindow() (last bool) {
	s.mu.Lock()
	defer s.mu.Unlock()
	log.Println("isLastWindow - len*s.windows = ", len(s.windows))
	if len(s.windows) < 1 {
		return true
	}
	return false
}



type goApplication struct {
	hInstance w32.HINSTANCE
	appName string
	iconName string
	winStack *goStack
	desktop *GoDeskTopWindow

}

var goApp *goApplication = nil

func GoApp()  (a *goApplication) {
	if goApp != nil {
		return goApp
	}
	return nil
}

func GoApplication(appName string) (a *goApplication) {
	if goApp == nil {
		ghInstance := w32.GetModuleHandle("")
		if appName == "" {
			appName = "Generic"
		}
		goApp = &goApplication{ghInstance, appName, appName, nil, nil}
		_, err := goApp.registerClass(ghInstance, appName, appName)
		if err != nil {
			log.Println("GoApplication::ERROR registering class")
		}
		goApp.winStack = &goStack{}
		goApp.winStack.controls = make(map[int] GoObject)
		goApp.winStack.windows = make(map[int] GoObject)
		goApp.winStack.index = make(map[w32.HWND] int)
		goApp.desktop = GoDeskTop()
		

	}
	return goApp
}

func (a *goApplication) AddControl(control GoObject, id int) {
	a.winStack.addControl(control , id)
}

func (a *goApplication) AddWindow(window GoObject, id int) {
	a.winStack.addWindow(window , id)
}

func (a *goApplication) FocusWindow() GoObject {
	return a.winStack.focusWindow()
}

func (a *goApplication) IsLastWindow() bool {
	return a.winStack.isLastWindow()
}

func (a *goApplication) RemoveControl(id int) {
	a.winStack.controls[int(id)].destroy()
	a.winStack.removeControl(id)
}

func (a *goApplication) RemoveWindow(id int) {
	a.winStack.windows[int(id)].destroy()
	a.winStack.removeWindow(id)
}

func (a *goApplication) SetWindowFocus(id int) {
	a.winStack.windowfocus = id
}

func (a *goApplication) AppName() string {
	return a.appName
}

func (a *goApplication) SetAppName(appName string) {
	a.appName = appName
}

func (a *goApplication) SetIconName(iconName string) {
	a.iconName = iconName
}

/*func (a *goApplication) WinStack() *GoStack {
	return a.winStack
}*/

func (a *goApplication) Controls(id uintptr) GoObject {
	return a.winStack.controls[int(id)]
}

func (a *goApplication) GoDeskTop() (hWin *GoDeskTopWindow) {
	return a.desktop
}

func (a *goApplication) ObjectType(object GoObject) (typ string) {
	typ = ""
	switch object.(type) {
		//case *GoMainWindowView:
			//typ = "GoMainWindow"
		//case *GoButtonControl:
			//typ = "GoButton"
	}
	return
}

func (a *goApplication) Run() (uint32) {
	var msg w32.MSG
	log.Println("running")
	for {
		//log.Println(".")
		msg = w32.MSG{}

		gotMessage, err := w32.GetMessage(&msg, 0, 0, 0)
		if err != nil {
			log.Println(err)
			return 62
		}

		if gotMessage {
			//log.Println("msg")
			w32.TranslateMessage(&msg)
			w32.DispatchMessage(&msg)
		} else {
			break
		}
	}
	return 0
}


// Top Level Default Application Proc
func (a *goApplication) DefWinProc(hWnd w32.HWND, msg uint32, wParam uintptr, lParam uintptr) uintptr {
	//wParamH := (wParam & 0xFFFF0000) >> 16
	//wParamL := wParam & 0xFFFF
	lParamH := (lParam & 0xFFFFFFFF00000000) >> 32
	lParamL := lParam & 0xFFFFFFFF

	mouseX := int(lParamL)
	mouseY := int(lParamH)
	win := goApp.FocusWindow() // Window->GoObject which has current windows focus
	

	switch msg {
		case w32.WM_ACTIVATE:
			hWnd := w32.HWND(lParamL)
			if id, ok := goApp.winStack.index[hWnd]; ok {
				if id < 100 {
					active := wParam != 0
					if active {
						a.SetWindowFocus(id)
						//w32.SetFocus(hWnd)
					}
					//return 0
				}
			}
			return 0
		case w32.WM_CLOSE:
			log.Println("WM_CLOSE")
			//if mw.onCanClose != nil {
			//	if mw.onCanClose() == false {
			//		return 0
			//	}
			//}
			w := win.wid()
			//if w.Parent() != nil {
			//	w32.EnableWindow(w.hWndParent, true)
			//	w32.SetForegroundWindow(w.hWndParent)
			//}
			if w.onClose != nil {
				w.onClose()
			}
			
		
		case w32.WM_COMMAND:
			win.onWM_COMMAND(wParam, lParam)
			return 0
		case w32.WM_DESTROY:
			log.Println("WM_DESTROY")

			if id, ok := goApp.winStack.index[hWnd]; ok {
				log.Println("WM_DESTROY::remove() - ", id)
				if id < 100 {
					a.RemoveWindow(id)
				} else {
					a.RemoveControl(id)
				}
			}
			if goApp.IsLastWindow() {
				log.Println("WM_DESTROY::isLastWindow")
				w32.PostQuitMessage(0)
			}
			return 0	
			//switch uint32(wparam) {
			//	case IDM_DOIT:
			//		hDC = w32.GetDC(hWnd)
			//		w32.TextOut(hDC, 10, 20, "Ok, I did it!")
			//		w32.ReleaseDC(hWnd, hDC)
			//	case IDM_QUIT:
			//		w32.DestroyWindow(hWnd)
			//}

		case w32.WM_KEYDOWN:
			w := win.wid()
			if w.onKeyDown != nil {
				w.onKeyDown(int(wParam))
				return 0
			}
		case w32.WM_KEYUP:
			w := win.wid()
			if w.onKeyUp != nil {
				w.onKeyUp(int(wParam))
				return 0
			}
		case w32.WM_LBUTTONDOWN, w32.WM_MBUTTONDOWN, w32.WM_RBUTTONDOWN:
			w := win.wid()
			if w.onMouseDown != nil {
				b := MouseButtonLeft
				if msg == w32.WM_MBUTTONDOWN {
					b = MouseButtonMiddle
				}
				if msg == w32.WM_RBUTTONDOWN {
					b = MouseButtonRight
				}
				w.onMouseDown(b, mouseX, mouseY)
			}
			return 0
		case w32.WM_LBUTTONUP, w32.WM_MBUTTONUP, w32.WM_RBUTTONUP:
			w := win.wid()
			if w.onMouseUp != nil {
				b := MouseButtonLeft
				if msg == w32.WM_MBUTTONUP {
					b = MouseButtonMiddle
				}
				if msg == w32.WM_RBUTTONUP {
					b = MouseButtonRight
				}
				w.onMouseUp(b, mouseX, mouseY)
			}
			return 0
		case w32.WM_MOUSEMOVE:
			w := win.wid()
			if w.onMouseMove != nil {
				w.onMouseMove(mouseX, mouseY)
				return 0
			}
		
	}
	return w32.DefWindowProc(hWnd, msg, wParam, lParam)
}

func (a *goApplication) registerClass(hInstance w32.HINSTANCE, appName string, iconName string) (atom w32.ATOM, err error) {
	var wndClass w32.WNDCLASSEX

	var icon w32.HICON
	var icon_err error

	icon, icon_err = w32.LoadIcon(hInstance, iconName)
	log.Println("(*goApplication) registerClass - LoadIcon:", icon_err)

	wndClass = w32.WNDCLASSEX{}
	wndClass.Style = w32.CS_HREDRAW | w32.CS_VREDRAW
	wndClass.WndProc = syscall.NewCallback(a.DefWinProc)
	wndClass.ClsExtra = 0
	wndClass.WndExtra = 0
	wndClass.Instance = hInstance
	if icon_err.Error() == "The operation completed successfully." {
		wndClass.Icon = icon
	} else {
		icon , icon_err = w32.LoadStockIcon(0, w32.IDI_APPLICATION)
		if icon_err.Error() == "The operation completed successfully." {
			wndClass.Icon = icon
		} else {
			return 0, icon_err
		}
	}
	
	wndClass.Cursor = w32.LoadCursor(0, w32.IDC_ARROW)
	wndClass.Background = w32.HBRUSH(w32.GetStockObject(w32.WHITE_BRUSH))
	wndClass.MenuName = syscall.StringToUTF16Ptr(appName)
	wndClass.ClassName = syscall.StringToUTF16Ptr(appName)
	
	wndClass.Size = uint32(unsafe.Sizeof(wndClass))

	atom, err = w32.RegisterClassEx(&wndClass)
	return atom, err
}