/* checkbox.go */

package win32

import (
	"log"
	"gitlab.com/win32go/source/win32/w32"
)

type GoCheckBoxControl struct {
	goWidget
	goObject
	checked  bool
	onChange func(bool)
}

func (c *GoCheckBoxControl) Checked() bool {
	return c.checked
}

func (c *GoCheckBoxControl) SetChecked(checked bool) {
	if checked == c.checked {
		return
	}
	c.checked = checked
	if c.hWnd != 0 {
		w32.SendMessage(c.hWnd, w32.BM_SETCHECK, toCheckState(c.checked), 0)
	}
	if c.onChange != nil {
		c.onChange(c.checked)
	}
	return
}

func toCheckState(checked bool) uintptr {
	if checked {
		return w32.BST_CHECKED
	}
	return w32.BST_UNCHECKED
}

func (c *GoCheckBoxControl) SetOnChange(f func(checked bool)) {
	c.onChange = f
}

func (c *GoCheckBoxControl) destroy() {
	/*for id, control := range w.controls {
		delete(w.controls, id)
	}*/
}

func (c *GoCheckBoxControl) wid() (*goWidget) {
	return &c.goWidget
}

func (c *GoCheckBoxControl) handleNotification(cmd uintptr) {
	log.Println("GoButtonControl::handleNotification..............")
	//buf := bufio.NewReader(os.Stdin)
    //buf.ReadBytes('\n')
	if cmd == w32.BN_CLICKED {
		c.checked = w32.SendMessage(c.hWnd, w32.BM_GETCHECK, 0, 0) == w32.BST_CHECKED
		if c.onChange != nil {
			c.onChange(c.checked)
		}
	}
}