/* core.go */

package win32

// win namespace

type MouseButton int

const (
	MouseButtonLeft MouseButton = iota
	MouseButtonMiddle
	MouseButtonRight
)

const (
	GO_AlignLeft = 0
	GO_AlignCentre = 1
	GO_AlignRight = 2
)

type GoRect struct {
	x int
	y int
	width int
	height int
}