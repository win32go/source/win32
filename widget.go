/* widget.go */

package win32

import (
	"log"
	//"syscall"
	//"unsafe"
	"gitlab.com/win32go/source/win32/w32"
)

type goWidget struct {
	className 	string
	windowName 	string
	style 		uint32
	exStyle     uint32
	state       int32
	x       	int
	y       	int
	width   	int
	height  	int
	hWndParent 	w32.HWND
	id 			int
	instance 	w32.HINSTANCE
	param		uint32
	//hAnchor  Anchor
	//vAnchor  Anchor
	hWnd 		w32.HWND
	parent  	*goWidget
	disabled 	bool
	visible   	bool
	//cursor    w32.HCURSOR / *goCursor
	font        *goFont
	//text 	 	string
	//window      bool
	widgets  map[int]*goWidget
	//alpha 		uint8
	onShow        func()
	onClose       func()
	onCanClose    func() bool
	onMouseMove   func(x, y int)
	onMouseWheel  func(x, y int, delta float64)
	onMouseDown   func(button MouseButton, x, y int)
	onMouseUp     func(button MouseButton, x, y int)
	onKeyDown     func(key int)
	onKeyUp       func(key int)
	onResize      func()
}

/*type iWidget interface {
	Create()
	Hide()
	IsVisible()
	IsWindow()
	SetVisible()
	Show()
	Update()
}*/

func (w *goWidget) Close() {
	if w.hWnd != 0 {
		w32.SendMessage(w.hWnd, w32.WM_CLOSE, 0, 0)
	}
}

func (w *goWidget) Create() (*goWidget, error) {
	var menu w32.HMENU
	var hWnd w32.HWND
	var err error
	
	// if widget has no parent then it is top level window
	// and has to have HMenu declared or nil (0)
	if w.parent == nil {
		menu = w32.HMENU(0)
	} else {
		menu = w32.HMENU(w.id)
	}
	hWnd, err = w32.CreateWindowExStr(
		w.exStyle,
		w.className,
		w.windowName,
		w.style,
		w.x, w.y, w.width, w.height,
		w.hWndParent,
		menu,
		w.instance,
		nil,
	)
		//w.className, w.windowName, w.style, w.x, w.y, w.width, w.height, w.hWndParent, w32.HMENU(w.id), w.instance)
	
	if err != nil {
		log.Println("widget id = ", w.id)
		log.Println("Failed to create widget.")
	} else {
		w.hWnd = hWnd
		// Top Level Window does not have parent
		if w.parent != nil {
			w.parent.widgets[w.id] = w
		}
	}

	return w, err
}

/*func (w *goWidget) handleNotification(cmd uintptr) {
	log.Println("GoWidget::handleNotification()")
}*/

func (w *goWidget) Hide() {
	if w32.ShowWindow(w.hWnd, w32.SW_HIDE) == false {
		w.state = w32.SW_HIDE
		w.visible = false
	}
}

func (w *goWidget) IsVisible() (bool) {
	w.visible = w32.IsWindowVisible(w.hWnd)
	return w.visible
}

func (w *goWidget) IsWindow() (window bool) {
	window = w32.IsWindow(w.hWnd)
	return window
}

func (w *goWidget) SetVisible(visible bool) () {
	if visible == true {
		w.Show()
	} else {
		w.Hide()
	}	
}

/*func (w *goWidget) SetState(s uint) {
	w.state = s
	if w.handle != 0 {
		w32.ShowWindow(w.handle, w.state)
	}
}*/

/*func (w *goWidget) State() {
	var p w32.WINDOWPLACEMENT
	if w32.GetWindowPlacement(w.handle, &p) {
		w.state = int(p.ShowCmd)
	}
}*/

func (w *goWidget) Show() {
	if w32.ShowWindow(w.hWnd, w32.SW_SHOW) == true {
		w.state = w32.SW_SHOW
		w.visible = true
	}
}

func (w *goWidget) ShowNormal() {
	if w32.ShowWindow(w.hWnd, w32.SW_SHOWNORMAL) == true {
		w.state = w32.SW_NORMAL
		w.visible = true
	}
}

func (w *goWidget) ShowMaximized() {
	if w32.ShowWindow(w.hWnd, w32.SW_MAXIMIZE) == true {
		w.state = w32.SW_MAXIMIZE
		w.visible = true
	}
}

func (w *goWidget) ShowMinimized() {
	if w32.ShowWindow(w.hWnd, w32.SW_MINIMIZE) == true {
		w.state = w32.SW_MINIMIZE
		w.visible = true
	}
}

func (w *goWidget) Font() *goFont {
	return w.font
}

func (w *goWidget) X() (x int) {
	return w.x
}

func (w *goWidget) Y() (y int) {
	return w.y
}

func (w *goWidget) Parent() *goWidget {
	return w.parent
}

func (w *goWidget) Position() (x int, y int) {
	return w.x, w.y
}

func (w *goWidget) Width() (width int) {
	return w.width
}

func (w *goWidget) Height() (height int) {
	return w.height
}

func (w *goWidget) OnResize() func() {
	return w.onResize
}

func (w *goWidget) Size() (width int, height int) {
	return w.width, w.height
}

func (w *goWidget) Style() uint32 { return w.style }

func (w *goWidget) SetStyle(ws uint32) {
	w.style = ws
	if w.hWnd != 0 {
		w32.SetWindowLongPtr(w.hWnd, w32.GWL_STYLE, uintptr(w.style))
		w32.ShowWindow(w.hWnd, w.state) // for the new style to take effect
		w.style = uint32(w32.GetWindowLongPtr(w.hWnd, w32.GWL_STYLE))
		//w.readBounds()
	}
}

func (w *goWidget) ExtendedStyle() uint32 { return w.exStyle }

func (w *goWidget) SetExtendedStyle(x uint32) {
	w.exStyle = x
	if w.hWnd != 0 {
		w32.SetWindowLongPtr(w.hWnd, w32.GWL_EXSTYLE, uintptr(w.exStyle))
		w32.ShowWindow(w.hWnd, w.state) // for the new style to take effect
		w.exStyle = uint32(w32.GetWindowLongPtr(w.hWnd, w32.GWL_EXSTYLE))
		//w.readBounds()
	}
}



func (w *goWidget) SetFont(f *goFont) {
	w.font = f
	/*for _, c := range w.widgets {
		c.parentFontChanged()
	}*/
}

func (w *goWidget) SetX(x int) {
	w.x = x
	w.setGeometry()
}

func (w *goWidget) SetY(y int) {
	w.y = y
	w.setGeometry()
}

func (w *goWidget) SetPosition(x int, y int) {
	w.x = x; w.y = y
	w.setGeometry()
}



func (w *goWidget) SetHeight(height int) {
	w.height = height
	w.setGeometry()
}

// SetOnCanClose is passed a function that is called when the window is about to
// be closed, e.g. when the user hits Alt+F4. If f returns true the window is
// closed, if f returns false, the window stays open.
func (w *goWidget) SetOnCanClose(f func() bool) {
	w.onCanClose = f
}

func (w *goWidget) SetOnClose(f func()) {
	w.onClose = f
}

func (w *goWidget) SetOnKeyDown(f func(key int)) {
	w.onKeyDown = f
}

func (w *goWidget) SetOnKeyUp(f func(key int)) {
	w.onKeyUp = f
}

func (w *goWidget) SetOnMouseDown(f func(button MouseButton, x, y int)) {
	w.onMouseDown = f
}

func (w *goWidget) SetOnMouseMove(f func(x, y int)) {
	w.onMouseMove = f
}

func (w *goWidget) SetOnMouseWheel(f func(x, y int, delta float64)) {
	w.onMouseWheel = f
}

func (w *goWidget) SetOnMouseUp(f func(button MouseButton, x, y int)) {
	w.onMouseUp = f
}

func (w *goWidget) SetOnResize(f func()) {
	w.onResize = f
}

func (w *goWidget) SetOnShow(f func()) {
	w.onShow = f
}

func (w *goWidget) SetSize(width int, height int) {
	
	w.width = width; w.height = height
	w.setGeometry()
}

func (w *goWidget) SetWidth(width int) {
	w.width = width
	w.setGeometry()
}

func (w *goWidget) Update() {
	w32.UpdateWindow(w.hWnd)
}

func (w *goWidget) extents() (rect GoRect) {
	rect = GoRect{w.x, w.y, w.width, w.height}
	return
}

func (w *goWidget) setGeometry() {
	if w.width < 0 {
		w.width = 0
	}
	if w.height < 0 {
		w.height = 0
	}
	if w.hWnd != 0 {
		// The window will receive a WM_SIZE which will handle anchoring child
		// controls.
		w32.SetWindowPos(
			w.hWnd, 0,
			w.x, w.y, w.width, w.height,
			w32.SWP_NOOWNERZORDER|w32.SWP_NOZORDER,
		)
	/*} else {
		w.clientWidth, w.clientHeight = w.InnerSize()
		w.x = x
		w.y = y
		w.width = width
		w.height = height
		w.repositionChidrenByAnchors()*/
	}
}