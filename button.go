/* button.go */

package win32

import (
	//"bufio"
	"log"
	//"os"
	//"syscall"
	//"unsafe"
	"gitlab.com/win32go/source/win32/w32"
)

type GoButtonControl struct {
	goWidget
	goObject
	onClick func()
	onDisable func()
	onDoubleClick func()
	onHiLite func()
	onPaint func()
	onUnHiLite func()
}

func (b *GoButtonControl) OnClick() func() {
	return b.onClick
}

func (b *GoButtonControl) SetOnClick(f func()) {
	b.onClick = f
}

func (b *GoButtonControl) OnDisable() func() {
	return b.onDisable
}

func (b *GoButtonControl) SetOnDisable(f func()) {
	b.onDisable = f
}

func (b *GoButtonControl) OnDoubleClick() func() {
	return b.onDoubleClick
}

func (b *GoButtonControl) SetOnDoubleClick(f func()) {
	b.onDoubleClick = f
}

func (b *GoButtonControl) OnHiLite() func() {
	return b.onHiLite
}

func (b *GoButtonControl) SetOnHiLite(f func()) {
	b.onHiLite = f
}

func (b *GoButtonControl) OnPaint() func() {
	return b.onPaint
}

func (b *GoButtonControl) SetOnPaint(f func()) {
	b.onPaint = f
}

func (b *GoButtonControl) OnUnHiLite() func() {
	return b.onUnHiLite
}

func (b *GoButtonControl) SetOnUnHiLite(f func()) {
	b.onUnHiLite = f
}
/*func (b *goButtonControl) Create() {
	b.goWidget.Create()
}*/

/*func (b *GoButtonControl) Show() {
	w32.ShowWindow(b.hWnd, w32.SW_SHOW)
}*/

/*func (b *goButtonControl) Update() {
	w32.UpdateWindow(b.hWnd)
}*/

/*func (b *GoButtonControl) obj() (*goObject) {
	return &b.goObject
}*/

/*func (b *GoButtonControl)addControl(id int, control GoObject) {
	b.goObject.addControl(id, control) 
}

func (b *GoButtonControl)control(id int) (GoObject) {
	return b.goObject.control(id) 
}

func (b *GoButtonControl)parentControl() (GoObject) {
	return b.goObject.parentControl() 
}

func (b *GoButtonControl)removeControl(control GoObject) {
	b.goObject.removeControl(control)
}*/

func (b *GoButtonControl) destroy() {
	/*for id, control := range w.controls {
		delete(w.controls, id)
	}*/
}

func (b *GoButtonControl) wid() (*goWidget) {
	return &b.goWidget
}

func (b *GoButtonControl) handleNotification(cmd uintptr) {
	log.Println("GoButtonControl::handleNotification..............")
	//buf := bufio.NewReader(os.Stdin)
    //buf.ReadBytes('\n')
	if cmd == w32.BN_CLICKED && b.onClick != nil {
		b.onClick()
	} else if cmd == w32.BN_DISABLE && b.onDisable != nil {
		b.onDisable()
	} else if cmd == w32.BN_DOUBLECLICKED && b.onDoubleClick != nil {
		b.onDoubleClick()
	} else if cmd == w32.BN_HILITE && b.onHiLite != nil {
		b.onHiLite()
	} else if cmd == w32.BN_PAINT && b.onPaint != nil {
		b.onPaint()
	} else if cmd == w32.BN_UNHILITE && b.onUnHiLite != nil {
		b.onUnHiLite()
	}
}